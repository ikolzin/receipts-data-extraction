[![pipeline status](https://gitlab.com/ikolzin/receipts-data-extraction/badges/main/pipeline.svg)](https://gitlab.com/ikolzin/receipts-data-extraction/-/pipelines) 

# Receipts Data Extraction

Extracts good and brand data from receipts.

------------

Companion projects:
 - [MLOps Stack](https://gitlab.com/ikolzin/receipts-data-extraction-mlops-stack)
 - [FastAPI service](https://gitlab.com/ikolzin/receipts-data-extraction-service)

------------

The project was originally developed for ['Structuring receipts with NLP'](https://ods.ai/competitions/nlp-receipts/leaderboard) competition. 

Competition datasets are available for free at [ods.ai](https://ods.ai): [stage 1](https://ods.ai/competitions/nlp-receipts/data), [stage 2](https://ods.ai/competitions/alfabank-nlp-receipts-2/dataset)


MLOps integrations developed thanks to [MLOps in production](https://ods.ai/tracks/ml-in-production-spring-23) course. 

------------

**Installation:**

1. Clone the repository to a local folder
2. Install [Poetry](https://python-poetry.org/docs/#installation)
3. run `poetry install`

You can now train a model and use it for inference (see [prepare_data.py](https://gitlab.com/ikolzin/receipts-data-extraction/-/blob/main/src/data/prepare_data.py?ref_type=heads), [train_model.py](https://gitlab.com/ikolzin/receipts-data-extraction/-/blob/main/src/model/train_model.py?ref_type=heads) and [predict.py](https://gitlab.com/ikolzin/receipts-data-extraction/-/blob/main/src/model/predict.py?ref_type=heads) docstrings for details).

You can also use DVC to run pipelines, example pipeline provided in [pipelines/train](https://gitlab.com/ikolzin/receipts-data-extraction/-/tree/main/pipelines/train?ref_type=heads).
To run all pipelines, use `dvc repro -R .`

4. (optional) Set up MLFlow stack: 
    - follow [instructions](https://gitlab.com/ikolzin/receipts-data-extraction-mlops-stack) to set up the stack
    - to connect to MLFlow from this project, rename [.env.template](https://gitlab.com/ikolzin/receipts-data-extraction/-/blob/main/.env.template?ref_type=heads) in the project root to .env and fill in your credentials. 

You can now run experiments and log results to MLFlow. See [run_experiments.py](https://gitlab.com/ikolzin/receipts-data-extraction/-/blob/main/src/experiments/mlflow/train_model_experiment.py?ref_type=heads) docstring for details.

5. (optional) Deploy a trained model using FastAPI:
    - folllow [instructions](https://gitlab.com/ikolzin/receipts-data-extraction-service)
