import ast
import logging
import shutil
from pathlib import Path
from typing import Literal

import click
import datasets
import pandas as pd
import transformers

from src.data.constants import (
    COLUMN_FORMATTED,
    COLUMN_TAGS,
    COLUMN_TOKENIZED_DESCRIPTION,
    COLUMN_TOKENIZED_TAGS,
    TOKEN_TO_IGNORE,
    index_to_tag,
    tag_to_index,
)


def train_model(
    data: pd.DataFrame,
    classification_method: Literal["tc", "mlm"],
    base_model_name: Path | str,
    output_dir: Path | str,
    num_epochs: int = 4,
    kfold_splits: int | None = None,
    test_size: float = 0.25,
    batch_size: int = 16,
    one_label_per_word: bool = False,
    seed: int | None = None,
    learning_rate: float = 2e-5,
    weight_decay: float = 0.01,
    delete_checkpoints: bool = False,
    early_stopping_patience: int | None = 3,
    resume_from_checkpoint: bool = False,
):
    """Fine-tunes the specified model on provided data.

    Args:
        data (pd.DataFrame): Data to train the model on.
        classification_method (str): Which classification method to use. Must be either "tc" or "mlm".
        base_model_name (Path | str): Base model to use (HF hub name or local path).
        output_dir (Path | str): Where to store the trained model.
        num_epochs (int, optional): How many epochs to train for. Defaults to 4.
        kfold_splits (int | None, optional): Whether to train multiple models for cross-validation, and number of cross-validation folds. Defaults to None.
        test_size (float, optional): Proportion of the dataset to allocate for test. Defaults to 0.25.
        batch_size (int, optional): Size of batches of data for model training. Defaults to 16.
        one_label_per_word (bool, optional): Whether to use one or multiple labels per word for token classification.
            Only applies when `classification_method` is 'tc'. Defaults to False.
        seed (int | None, optional): Set a seed for training. None to use HF defaults. Defaults to None.
        learning_rate (float, optional): Learning rate for the optimizer. Defaults to 2e-5.
        weight_decay (float, optional): Weight decay for the optimizer. Defaults to 0.01.
        delete_checkpoints (bool, optional): Whether to delete intermediate checkpoints after training. Defaults to False.
        early_stopping_patience (int | None, optional): Number of epochs with no improvement
            after which training will be stopped. Defaults to 3.
        resume_from_checkpoint (bool, optional): Whether to resume training from a checkpoint. Defaults to False.
    """
    assert isinstance(data, pd.DataFrame)
    assert isinstance(base_model_name, (Path, str))
    assert isinstance(output_dir, (Path, str))
    assert isinstance(num_epochs, int)
    assert num_epochs > 0
    assert isinstance(test_size, float)
    assert test_size > 0
    assert test_size < 1
    assert isinstance(batch_size, int)
    assert batch_size > 0
    assert isinstance(classification_method, str)
    assert classification_method in ["tc", "mlm"]
    assert isinstance(one_label_per_word, bool)
    assert (seed is None) or (seed in ["randomize"]) or isinstance(seed, int)
    assert isinstance(learning_rate, float)
    assert learning_rate > 0
    assert isinstance(weight_decay, float)
    assert weight_decay >= 0
    assert (kfold_splits is None) or (
        (isinstance(kfold_splits, int)) and (kfold_splits >= 1)
    )
    assert isinstance(delete_checkpoints, bool)
    assert (early_stopping_patience is None) or isinstance(early_stopping_patience, int)
    assert isinstance(resume_from_checkpoint, bool)

    if isinstance(seed, int):
        transformers.set_seed(seed)

    dset = datasets.Dataset.from_pandas(df=data)
    dsets = {}
    eval_results = []
    if (kfold_splits is None) or (kfold_splits == 1):
        dsets["main"] = dset.train_test_split(test_size, seed=seed)
    else:
        shard_indices = set(range(kfold_splits))
        for test_index in shard_indices:
            split_ds = datasets.DatasetDict(
                {
                    "test": dset.shard(kfold_splits, test_index, contiguous=True),
                    # train is everything except test shard
                    "train": datasets.concatenate_datasets(
                        [
                            dset.shard(kfold_splits, i, contiguous=True)
                            for i in shard_indices - {test_index}
                        ]
                    ),
                },
            )
            dsets[f"kfold_{test_index}"] = split_ds

    tokenizer = transformers.AutoTokenizer.from_pretrained(base_model_name)

    output_basedir = Path(output_dir)
    for name, dataset in dsets.items():
        if len(dsets) > 1:
            output_dir = output_basedir.joinpath(name)

        # prepare to train a token classification model
        if classification_method == "tc":
            data_collator = transformers.DataCollatorForTokenClassification(
                tokenizer=tokenizer
            )
            model = transformers.AutoModelForTokenClassification.from_pretrained(
                base_model_name,
                num_labels=len(index_to_tag),
                id2label=index_to_tag,
                label2id=tag_to_index,
            )
            dataset = dataset.map(
                _tokenize_for_token_classification,
                batched=True,
                fn_kwargs={
                    "tokenizer": tokenizer,
                    "one_label_per_word": one_label_per_word,
                },
            )

        # prepare to train a masked language modeling model
        elif classification_method == "mlm":
            data_collator = transformers.DataCollatorForLanguageModeling(
                tokenizer=tokenizer, mlm_probability=0.15
            )
            model = transformers.AutoModelForMaskedLM.from_pretrained(base_model_name)
            dataset = dataset.map(
                _tokenize_for_mlm,
                batched=True,
                fn_kwargs={
                    "tokenizer": tokenizer,
                },
            )

        else:
            raise Exception("Unknown classification method")

        eval_steps = int(6400 / batch_size)
        save_steps = eval_steps * 5
        training_args = transformers.TrainingArguments(
            output_dir=Path(output_dir).joinpath("checkpoints"),
            learning_rate=learning_rate,
            per_device_train_batch_size=batch_size,
            per_device_eval_batch_size=batch_size,
            num_train_epochs=num_epochs,
            weight_decay=weight_decay,
            evaluation_strategy=transformers.IntervalStrategy.STEPS,
            save_strategy=transformers.IntervalStrategy.STEPS,
            save_total_limit=1,
            load_best_model_at_end=True,
            # pushing large models to hub while training takes enourmous amounts of HDD space (100GB+)
            push_to_hub=False,
            hub_private_repo=True,
            eval_steps=eval_steps,
            save_steps=save_steps,
        )

        if seed is not None:
            training_args.seed = seed

        if early_stopping_patience is None:
            callbacks = None
        else:
            callbacks = [
                transformers.EarlyStoppingCallback(
                    early_stopping_patience=early_stopping_patience
                )
            ]

        trainer = transformers.Trainer(
            model=model,
            args=training_args,
            train_dataset=dataset["train"],
            eval_dataset=dataset["test"],
            tokenizer=tokenizer,
            data_collator=data_collator,
            callbacks=callbacks,
        )

        trainer.train(resume_from_checkpoint=resume_from_checkpoint)
        trainer.save_model(output_dir)
        if delete_checkpoints:
            shutil.rmtree(Path(output_dir).joinpath("checkpoints"), ignore_errors=True)
        eval_results.append(trainer.evaluate())

    return eval_results


def _tokenize_for_token_classification(
    examples,
    tokenizer: transformers.PreTrainedTokenizerFast,
    one_label_per_word: bool = True,
):
    tokenized_inputs = tokenizer(
        examples[COLUMN_TOKENIZED_DESCRIPTION],
        padding=True,
        is_split_into_words=True,
    )

    labels = []
    for i, label in enumerate(examples[COLUMN_TAGS]):
        word_ids = tokenized_inputs.word_ids(
            batch_index=i
        )  # Map tokens to their respective word.
        previous_word_idx = None
        label_ids = []
        for word_idx in word_ids:  # Set the special tokens to -100.
            if word_idx is None:
                label_ids.append(TOKEN_TO_IGNORE)
            else:
                if one_label_per_word and word_idx == previous_word_idx:
                    label_ids.append(TOKEN_TO_IGNORE)
                else:
                    label_ids.append(label[word_idx])
            previous_word_idx = word_idx
        labels.append(label_ids)

    tokenized_inputs[COLUMN_TOKENIZED_TAGS] = labels
    return tokenized_inputs


def _tokenize_for_mlm(examples, tokenizer: transformers.PreTrainedTokenizerFast):
    tokenized_inputs = tokenizer(
        examples[COLUMN_FORMATTED],
        padding=True,
    )
    return tokenized_inputs


@click.command()
@click.argument("data", type=click.Path(exists=True))
@click.argument("classification_method", type=click.Choice(["tc", "mlm"]))
@click.argument("base_model_name", type=str)
@click.argument("output_dir", type=click.Path())
@click.option(
    "--num-epochs",
    type=int,
    default=4,
    help="How many epochs to train for. Default is 4.",
)
@click.option(
    "--kfold-splits",
    type=int,
    help="Whether to train multiple models for cross-validation, and number of cross-validation folds. Defaults to None.",
)
@click.option(
    "--test-size",
    type=float,
    default=0.25,
    help="Proportion of the dataset to allocate for test. Default is 0.25.",
)
@click.option(
    "--batch-size",
    type=int,
    default=16,
    help="Size of batches of data for model training. Default is 16.",
)
@click.option(
    "--one-label-per-word",
    is_flag=True,
    help='Whether to use one or multiple labels per word for token classification. Only applies when `classification_method` is "tc". Defaults to False.',
)
@click.option(
    "--seed",
    type=int,
    help="Set a seed for training. Provide an integer for a fixed seed. None to use HF defaults. Defaults to None.",
)
@click.option(
    "--learning-rate",
    type=float,
    default=2e-5,
    help="Learning rate for the optimizer. Default is 2e-5.",
)
@click.option(
    "--weight-decay",
    type=float,
    default=0.01,
    help="Weight decay for the optimizer. Default is 0.01.",
)
@click.option(
    "--delete-checkpoints",
    is_flag=True,
    help="Whether to delete intermediate checkpoints after training. Defaults to False.",
)
@click.option(
    "--early-stopping-patience",
    type=int,
    help="Number of epochs with no improvement after which training will be stopped. Defaults to 3.",
)
@click.option(
    "--resume-from-checkpoint",
    is_flag=True,
    help="Whether to resume training from a checkpoint. Defaults to False.",
)
def train_model_cli(
    data,
    classification_method,
    base_model_name,
    output_dir,
    num_epochs,
    kfold_splits,
    test_size,
    batch_size,
    one_label_per_word,
    seed,
    learning_rate,
    weight_decay,
    delete_checkpoints,
    early_stopping_patience,
    resume_from_checkpoint,
):
    """
    Fine-tunes the specified model on provided data.
    """
    df = pd.read_csv(data, converters={1: ast.literal_eval, 2: ast.literal_eval})
    train_model(
        df,
        classification_method,
        base_model_name,
        output_dir,
        num_epochs,
        kfold_splits,
        test_size,
        batch_size,
        one_label_per_word,
        seed,
        learning_rate,
        weight_decay,
        delete_checkpoints,
        early_stopping_patience,
        resume_from_checkpoint,
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    try:
        logging.info("Starting train_model...")
        train_model_cli()
    except Exception as e:
        logging.error("Error while training model: ")
        logging.exception(e)
    finally:
        logging.info("Finished train_model.")
