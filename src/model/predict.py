import ast
import json
import logging
import os
from pathlib import Path
from typing import Literal

import click
import pandas as pd
import torch
import transformers
from pandarallel import pandarallel
from thefuzz import fuzz, process
from tqdm.auto import tqdm

from src.data.alphabets import (
    english_alphabet,
    english_to_russian,
    russian_alphabet,
    russian_to_english,
)
from src.data.constants import (
    COLUMN_BRAND,
    COLUMN_DESCRIPTION,
    COLUMN_GOOD,
    COLUMN_ID,
    COLUMN_TOKENIZED_DESCRIPTION,
    COLUMN_TOKENIZED_TAGS,
    TAG_BRAND_BEGINNING,
    TAG_BRAND_INSIDE,
    TAG_GOOD_BEGINNING,
    TAG_GOOD_INSIDE,
)


def predict(
    model_path: Path | str | list[Path | str],
    input_df: pd.DataFrame,
    classification_method: Literal["tc"] = "tc",
    output_file: Path | str | None = None,
    output_dir: Path | str | None = None,
    batch_size: int = 100,
    max_rows: int = 0,
    drop_duplicates: bool = True,
    brands_list_path: Path | str | None = None,
    brands_language_conversions: bool = True,
) -> None:
    """
    Runs the prediction pipeline. Takes the prepared pandas Dataframe as an input.
    Either output_file or output_dir must be provided.
    If output_file is specified, outputs predictions there.
    If output_dir is specified, writes two csv files to output_dir:
     - full.csv keeps all columns for analysis
     - submission.csv only keeps columns required to submit prediction for the competition

    Args:
        model_path (Path | str | list): Path to model(s) used for inference. Expects Huggingface-compatible model(s).
        input_df (pd.DataFrame): A prepared pandas Dataframe with values to run inference on.
        classification_method (["tc"]): Which classification method to use. Currently only token classification is supported.
        output_file (Path | str | None): Path to the csv file which will contain inference results. Must be specified if output_dir is None. Defaults to None.
        output_dir (Path | str | None): Path to directory which will contain result files. Ignored if output_file is not None. Must be specified if output_file is None. Defaults to None.
        batch_size (int, optional): How many rows to process at a time. Useful for VRAM-limited situations. Defaults to 100.
        max_rows (int, optional): Limit how many rows to process in total. 0 for no limit. Defaults to 0.
        drop_duplicates (bool, optional): Remove duplicate sentences from results. Defaults to True.
        brands_list_path (Path | str | None): Path to file containing brand names to look for. None means don't look. Large files take a lot of time to process. Defaults to None.
        brands_language_conversions (bool): Whether to try converting inputs to match brand language. Takes additional time to process if True. Only applicaple if brands_list_path is not None. Defaults to True.
    """
    assert isinstance(model_path, (Path, str, list))
    if isinstance(model_path, list):
        for m in model_path:
            assert isinstance(m, (Path, str))
    assert isinstance(input_df, pd.DataFrame)
    assert (output_dir is None) or (isinstance(output_dir, (Path, str)))
    assert (output_file is None) or (isinstance(output_file, (Path, str)))
    # Check that at least one of output_dir or output_file is specified
    assert not ((output_dir is None) and (output_file is None))
    assert isinstance(batch_size, int)
    assert isinstance(max_rows, int)
    assert isinstance(drop_duplicates, bool)
    assert (brands_list_path is None) or isinstance(brands_list_path, (Path, str))
    assert isinstance(brands_language_conversions, bool)
    assert isinstance(classification_method, str)
    assert classification_method in ["tc"]

    pandarallel.initialize(progress_bar=True)

    device = "cuda:0" if torch.cuda.is_available() else "cpu"

    if not isinstance(model_path, list):
        model_dir = Path(model_path)
        if model_dir.joinpath("config.json").exists():
            models = [model_path]
        else:
            models = []
            dir_items = sorted(os.listdir(model_dir))
            for item in dir_items:
                if (
                    model_dir.joinpath(item).is_dir()
                    and (item != "checkpoints")
                    and model_dir.joinpath(item).joinpath("config.json").exists()
                ):
                    models.append(model_dir.joinpath(item))
            if len(models) == 0:
                raise Exception(f"No config.json files found in {model_path}.")

    else:
        models = model_path

    df = input_df

    # limit max_rows for situations where we don't want to work with the entire dataset
    if max_rows != 0:
        df = df.iloc[:max_rows]

    # feed this to tokenizer in batches
    values = df[COLUMN_TOKENIZED_DESCRIPTION].values

    logit_batches_by_model = []
    for model_path in models:
        tokenizer = transformers.AutoTokenizer.from_pretrained(model_path)
        model = transformers.AutoModelForTokenClassification.from_pretrained(model_path)
        model = model.to(device)

        current_batch = 0
        logit_batches = []
        predicted_classes = []
        word_ids = []
        for current_batch in tqdm(range(0, values.shape[0], batch_size)):
            # convert np.array to list to feed to the tokenizer
            batch = values[current_batch : current_batch + batch_size].tolist()
            # get tensors to run model on
            inputs = tokenizer(
                batch, return_tensors="pt", padding=True, is_split_into_words=True
            ).to(device)
            # get ids of words to later match tokens with words
            word_ids.extend(
                [inputs.word_ids(i) for i in range(len(inputs["input_ids"]))]
            )

            with torch.no_grad():
                logits = model(**inputs).logits

            logit_batches.append(logits)

        logit_batches_by_model.append(logit_batches)

    stacked_batches = [torch.stack(t) for t in zip(*logit_batches_by_model)]
    mean_batches = [batch.mean(dim=0) for batch in stacked_batches]

    for logit_batch in mean_batches:
        predictions = torch.argmax(logit_batch, dim=2).to(device)

        # convert predictions from tensors to readable tags
        predicted_classes.extend(
            [[model.config.id2label[e.item()] for e in t] for t in predictions]
        )
    df[COLUMN_TOKENIZED_TAGS] = predicted_classes
    df["word_ids"] = word_ids

    # if dataset already has target columns - rename them (we keep them to run scoring)
    if COLUMN_GOOD in df.columns:
        df.rename(columns={COLUMN_GOOD: "orig_good"}, inplace=True)
    if COLUMN_BRAND in df.columns:
        df.rename(columns={COLUMN_BRAND: "orig_brand"}, inplace=True)

    brands = None
    if brands_list_path is not None:
        with open(brands_list_path, "r") as f:
            brands = json.load(f)
        brands = set(brands)
        logging.info(f"Loaded {len(brands)} brands for lookup.")

    df = df.parallel_apply(
        _find_goods_and_brands,
        axis=1,
        drop_duplicates=drop_duplicates,
        brands=brands,
        brands_language_conversions=brands_language_conversions,
    )

    # write prediction results
    if output_file is not None:
        df[[COLUMN_ID, COLUMN_GOOD, COLUMN_BRAND]].to_csv(output_file, index=False)
    else:
        os.makedirs(output_dir, exist_ok=True)
        df.to_csv(Path(output_dir).joinpath("full.csv"), index=False)

        df[[COLUMN_ID, COLUMN_GOOD, COLUMN_BRAND]].to_csv(
            Path(output_dir).joinpath("submission.csv"), index=False
        )


def _make_sentences(
    row, tag_beginning: str, tag_inside: str, drop_duplicates: bool = False
) -> str:
    sentences = []
    current_sentence = None
    current_word_id = None
    for label, word_id in zip(row[COLUMN_TOKENIZED_TAGS], row["word_ids"]):
        if word_id is None:
            # can't do anything with labels not assigned to words
            continue
        if label == tag_beginning:
            # words with this tag always start new sentences
            if word_id != current_word_id:
                # add completed sentence to sentences
                if current_sentence is not None:
                    sentences.append(current_sentence)
                current_sentence = row[COLUMN_TOKENIZED_DESCRIPTION][word_id]
                current_word_id = word_id
        elif label == tag_inside:
            # words with this tag should be preceded by other words in a sentence
            # but sometimes model fails to do so
            if word_id != current_word_id:
                # add a word to the current sentence
                if current_sentence is not None:
                    current_sentence = " ".join(
                        [current_sentence, row[COLUMN_TOKENIZED_DESCRIPTION][word_id]]
                    )
                else:
                    current_sentence = row[COLUMN_TOKENIZED_DESCRIPTION][word_id]
                current_word_id = word_id
        else:
            if (current_word_id is not None) and (word_id != current_word_id):
                # we moved on to the next word and it is not of a wanted tag
                # add completed sentence to sentences
                if current_sentence is not None:
                    sentences.append(current_sentence)
                current_sentence = None
                current_word_id = None
    if current_sentence is not None:
        sentences.append(current_sentence)

    if drop_duplicates:
        seen = set()
        sentences = [s for s in sentences if not (s in seen or seen.add(s))]

    return ",".join(sentences)


def _scorer(s1, s2):
    return fuzz.WRatio(s1, s2, force_ascii=False, full_process=False)


def _find_goods_and_brands(
    row,
    search_tags=True,
    drop_duplicates=False,
    brands=None,
    brands_language_conversions=True,
):
    if search_tags:
        row[COLUMN_GOOD] = _make_sentences(
            row,
            tag_beginning=TAG_GOOD_BEGINNING,
            tag_inside=TAG_GOOD_INSIDE,
            drop_duplicates=drop_duplicates,
        )
        row[COLUMN_BRAND] = _make_sentences(
            row,
            tag_beginning=TAG_BRAND_BEGINNING,
            tag_inside=TAG_BRAND_INSIDE,
            drop_duplicates=drop_duplicates,
        )

    # if model didn't find a brand - try looking for it in the dictionary
    if (row[COLUMN_BRAND] == "") and (brands is not None):
        s = row[COLUMN_DESCRIPTION].lower()
        if brands_language_conversions:
            rus_to_eng = "".join(
                [russian_to_english[c] if c in russian_alphabet else c for c in s]
            )
            eng_to_rus = "".join(
                [english_to_russian[c] if c in english_alphabet else c for c in s]
            )
            # jam all variants together separated by spaces and search for a brand in any of them
            s = " ".join(
                [
                    s,
                    rus_to_eng,
                    eng_to_rus,
                ]
            )

        found_brand, score = process.extractOne(
            s, brands, scorer=_scorer, processor=None
        )
        if score >= 90:
            row[COLUMN_BRAND] = found_brand
    return row


@click.command()
@click.option(
    "--model-path",
    type=click.Path(exists=True),
    required=True,
    help="Path to the model(s) used for inference.",
)
@click.option(
    "--input-file",
    type=click.Path(exists=True),
    required=True,
    help="Path to the input file in CSV format.",
)
@click.option(
    "--output-file", type=click.Path(), help="Path to the output file in CSV format."
)
@click.option(
    "--output-dir",
    type=click.Path(),
    help="Path to the output directory where result files will be stored.",
)
def predict_cli(model_path, input_file, output_file, output_dir):
    if output_file and output_dir:
        raise click.UsageError(
            "Either output-file or output-dir must be provided, not both."
        )

    if not output_file and not output_dir:
        raise click.UsageError("Either output-file or output-dir must be provided.")

    try:
        logging.info("Starting predict...")
        input_df = pd.read_csv(input_file, converters={1: ast.literal_eval})
        predict(
            model_path=Path(model_path),
            input_df=input_df,
            output_file=Path(output_file) if output_file else None,
            output_dir=Path(output_dir) if output_dir else None,
        )
        logging.info("Prediction completed successfully.")
    except Exception as e:
        logging.error("Error while predicting: ")
        logging.exception(e)
    finally:
        logging.info("Finished predict.")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    predict_cli()
