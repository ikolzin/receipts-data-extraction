import string

english_alphabet = set(string.ascii_lowercase)
russian_alphabet = set("абвгдеёжзийклмнопрстуфхцчшщъыьэюя")

likely_english_mistakes = set("abcekmnoprtuxy")
likely_russian_mistakes = set("авгекопрстух")

# this is intended to fix image recognition mistakes, not for transliteration
# letters with no close shapes are left as transliterations for completeness
english_to_russian = {
    "a": "а",
    "b": "в",  # B
    "c": "с",
    "d": "д",
    "e": "е",
    "f": "е",  # Е is most similar to F, maybe?
    "g": "д",  # cursive д looks like g
    "h": "н",  # H
    "i": "и",
    "j": "у",  # J
    "k": "к",
    "l": "л",
    "m": "м",
    "n": "п",
    "o": "о",
    "p": "р",
    "q": "о",  # Q
    "r": "г",
    "s": "с",
    "t": "т",
    "u": "и",
    "v": "и",
    "w": "ш",
    "x": "х",
    "y": "у",
    "z": "з",
}

russian_to_english = {
    "а": "a",
    "б": "b",
    "в": "b",  # B
    "г": "r",
    "д": "d",
    "е": "e",
    "ё": "e",
    "ж": "w",
    "з": "z",
    "и": "u",
    "й": "u",
    "к": "k",
    "л": "l",
    "м": "m",
    "н": "h",  # H
    "о": "o",
    "п": "n",
    "р": "p",
    "с": "c",
    "т": "t",
    "у": "y",
    "ф": "o",
    "х": "x",
    "ц": "u",
    "ч": "y",
    "ш": "w",
    "щ": "w",
    "ъ": "b",
    "ы": "bl",
    "ь": "b",
    "э": "e",
    "ю": "lo",
    "я": "r",
}

translit_english_to_russian = {
    "a": "а",
    "b": "б",
    "c": "ц",
    "d": "д",
    "e": "е",
    "f": "ф",
    "g": "г",
    "h": "х",
    "i": "и",
    "j": "ж",
    "k": "к",
    "l": "л",
    "m": "м",
    "n": "н",
    "o": "о",
    "p": "п",
    "q": "к",
    "r": "р",
    "s": "с",
    "t": "т",
    "u": "у",
    "v": "в",
    "w": "в",
    "x": "кс",
    "y": "и",
    "z": "з",
}

translit_russian_to_english = {
    "а": "a",
    "б": "b",
    "в": "v",
    "г": "g",
    "д": "d",
    "е": "e",
    "ё": "yo",
    "ж": "j",
    "з": "z",
    "и": "i",
    "й": "j",
    "к": "k",
    "л": "l",
    "м": "m",
    "н": "n",
    "о": "o",
    "п": "p",
    "р": "r",
    "с": "s",
    "т": "t",
    "у": "u",
    "ф": "f",
    "х": "h",
    "ц": "ts",
    "ч": "ch",
    "ш": "sh",
    "щ": "sh",
    "ъ": "",
    "ы": "y",
    "ь": "",
    "э": "e",
    "ю": "u",
    "я": "ya",
}
