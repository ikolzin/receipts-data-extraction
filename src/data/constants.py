# input dataset columns
COLUMN_ID = "id"
COLUMN_DESCRIPTION = "name"
COLUMN_GOOD = "good"
COLUMN_BRAND = "brand"

# processsed data columns
COLUMN_TOKENIZED_DESCRIPTION = "tokens"
COLUMN_TAGS = "tags"
COLUMN_TOKENIZED_TAGS = "labels"

# tags
TAG_O = "O"
TAG_GOOD_BEGINNING = "B-good"
TAG_GOOD_INSIDE = "I-good"
TAG_BRAND_BEGINNING = "B-brand"
TAG_BRAND_INSIDE = "I-brand"

# MLM
COLUMN_FORMATTED = "formatted"
PH_DESCRIPTION = "description: "
PH_GOOD = "good: "
PH_BRAND = "brand: "
PH_SPLITTER = " ; "
MLM_MASK = "<mask>"

index_to_tag = {
    0: TAG_O,
    1: TAG_GOOD_BEGINNING,
    2: TAG_GOOD_INSIDE,
    3: TAG_BRAND_BEGINNING,
    4: TAG_BRAND_INSIDE,
}
tag_to_index = {
    TAG_O: 0,
    TAG_GOOD_BEGINNING: 1,
    TAG_GOOD_INSIDE: 2,
    TAG_BRAND_BEGINNING: 3,
    TAG_BRAND_INSIDE: 4,
}

# special tag values
TOKEN_TO_IGNORE = -100
