import logging
import re
from pathlib import Path
from typing import Literal

import click
import pandas as pd
from pandarallel import pandarallel

from src.data.alphabets import (
    english_alphabet,
    english_to_russian,
    likely_english_mistakes,
    likely_russian_mistakes,
    russian_alphabet,
    russian_to_english,
)
from src.data.constants import (  # noqa 401
    COLUMN_BRAND,
    COLUMN_DESCRIPTION,
    COLUMN_FORMATTED,
    COLUMN_GOOD,
    COLUMN_ID,
    COLUMN_TAGS,
    COLUMN_TOKENIZED_DESCRIPTION,
    COLUMN_TOKENIZED_TAGS,
    MLM_MASK,
    PH_BRAND,
    PH_DESCRIPTION,
    PH_GOOD,
    PH_SPLITTER,
    TAG_BRAND_BEGINNING,
    TAG_BRAND_INSIDE,
    TAG_GOOD_BEGINNING,
    TAG_GOOD_INSIDE,
    TAG_O,
    TOKEN_TO_IGNORE,
    index_to_tag,
    tag_to_index,
)


def prepare_data(
    filepath: Path | str,
    classification_method: Literal["tc", "mlm"],
    use_targets: bool = True,
    remove_uncommon_characters=True,
    fix_languages=True,
    check_likely_mistakes: bool = True,
    drop_if_mismatched: Literal["never", "all", "goods", "brands", "any"] = "never",
    drop_input_columns=True,
    desc_split_regex: str = r"[^a-zа-яё'&]+",
    desc_strip_chars: str = "'\"&",
) -> pd.DataFrame:
    """
    Prepare the data for token classification or masked language modeling.

    Args:
        filepath (Path | str): Path to the csv containing original data.
        classification_method ("tc" | "mlm"): The classification method to use.
        use_targets (bool, optional): Whether to use data from columns. Only applicable when classification_method == "mlm". Defaults to True.
        remove_uncommon_characters (bool, optional): Whether to relace some uncommon characters with more common version. Defaults to True.
        fix_languages (bool, optional): Whether to attempt to fix words containing characters from different alphabets. Defaults to True.
        check_likely_mistakes (bool, optional): Whether to pay more attention to characters more likely to be misread while fixing languages.
            Only applicable if fix_languages is True. Defaults to True.
        drop_if_mismatched ("never" | "all" | "goods" | "brands" | "any", optional):
            Whether to drop rows where description doesn't contain goods or brands. Only applicable when good or brand data is provided.
            - "never": don't drop any rows
            - "all": drop rows where description contains neither goods nor brands
            - "goods" drop rows where description doesn't contain goods
            - "brands" drop rows where description doesn't contain brands
            - "any" drop rows where description contains only goods or brands but not both
            Defaults to "never".
        drop_input_columns (bool, optional): Whether to drop the input columns after processing. Defaults to True.
        desc_split_regex (str, optional): The regex pattern used to split the description into words. Defaults to r"[^a-zа-яё'&]+".
        desc_strip_chars (str, optional): The characters to strip from words made from the description. Defaults to "'\"&".

    Returns:
        pd.DataFrame: The prepared data.

    Raises:
        AssertionError: If any of the input arguments have invalid types or values.

    """
    assert isinstance(filepath, (Path, str))
    assert isinstance(classification_method, str)
    assert classification_method in ["tc", "mlm"]
    assert isinstance(use_targets, bool)
    assert isinstance(remove_uncommon_characters, bool)
    assert isinstance(fix_languages, bool)
    assert isinstance(check_likely_mistakes, bool)
    assert isinstance(drop_input_columns, bool)
    assert isinstance(desc_split_regex, str)
    assert isinstance(desc_strip_chars, str)
    assert isinstance(drop_if_mismatched, str)
    assert drop_if_mismatched in ["never", "all", "goods", "brands", "any"]

    pandarallel.initialize(progress_bar=False)

    # make a Pandas dataframe from raw data with NaN values replaced with empty strings
    df = pd.read_csv(filepath).fillna("")
    # make a 'tokens' column by splitting 'name' column into words
    lowercase_description = df[COLUMN_DESCRIPTION].str.lower()
    if remove_uncommon_characters:
        split_description = lowercase_description.parallel_apply(
            _remove_uncommon_characters,
            desc_strip_chars=desc_strip_chars,
            desc_split_regex=desc_split_regex,
        )
    else:
        split_description = lowercase_description.str.split()
    df[COLUMN_TOKENIZED_DESCRIPTION] = split_description

    # fix words which consist of letters from different alphabets
    if fix_languages:
        df = df.parallel_apply(
            _fix_word_languages, axis=1, check_likely_mistakes=check_likely_mistakes
        )

    # if data has 'good' or 'brand' rows - we can use it for training
    has_targets = (COLUMN_GOOD in df.columns) or (COLUMN_BRAND in df.columns)

    # remove rows which have goods/brands specified, but not in description
    if drop_if_mismatched == "never":
        pass
    else:
        df["mismatched_goods"] = df.parallel_apply(_mark_mismatched_goods, axis=1)
        df["mismatched_brands"] = df.parallel_apply(_mark_mismatched_brands, axis=1)
        if drop_if_mismatched == "all":
            query = "(mismatched_goods == True) and (mismatched_brands == True)"
        elif drop_if_mismatched == "goods":
            query = "mismatched_goods == True"
        elif drop_if_mismatched == "brands":
            query = "mismatched_brands == True"
        elif drop_if_mismatched == "any":
            query = "(mismatched_goods == True) or (mismatched_brands == True)"
        else:
            raise Exception(
                f"Unexpected drop_if_mismatched value: {drop_if_mismatched}."
            )
        rows_to_drop = df.query(query).index
        df.drop(rows_to_drop, inplace=True)
        df.drop("mismatched_goods", axis=1, inplace=True)
        df.drop("mismatched_brands", axis=1, inplace=True)

    # prepare data for token classification
    if classification_method == "tc":
        if has_targets and use_targets:
            df[COLUMN_TAGS] = df.parallel_apply(_apply_bio_tagging, axis=1)

    # prepare data for masked language modeling
    if classification_method == "mlm":
        df[COLUMN_FORMATTED] = df.parallel_apply(
            _format_for_mlm,
            axis=1,
            use_targets=(has_targets and use_targets),
            ph_description=PH_DESCRIPTION,
            ph_good=PH_GOOD,
            ph_brand=PH_BRAND,
            ph_splitter=PH_SPLITTER,
        )
        if drop_input_columns:
            df.drop(COLUMN_TOKENIZED_DESCRIPTION, axis=1, inplace=True)

    if drop_input_columns:
        df.drop(COLUMN_DESCRIPTION, axis=1, inplace=True)
        if has_targets:
            df.drop(COLUMN_GOOD, axis=1, inplace=True)
            df.drop(COLUMN_BRAND, axis=1, inplace=True)

    return df


def _remove_uncommon_characters(
    lowercase_description: str, desc_strip_chars, desc_split_regex
):
    s = lowercase_description
    # replace weird apostrophes with normal ones
    s = s.replace("`", "'")
    s = s.replace("’", "'")
    # replace ё with е
    s = s.replace("ё", "е")
    # split the string on provided regex
    # strip provided characters from both ends of resulting strings
    # remove empty strings from result
    result = [
        e.strip(desc_strip_chars)
        for e in re.split(desc_split_regex, s)
        if e.strip(desc_strip_chars)  # this evaluates to True if string is not empty
    ]
    return result


def _mark_mismatched_goods(row: pd.Series):
    goods = row[COLUMN_GOOD].replace(",", " ").split()
    mismatched_goods = set(row[COLUMN_TOKENIZED_DESCRIPTION]).isdisjoint(goods)
    return mismatched_goods and any(goods)


def _mark_mismatched_brands(row: pd.Series):
    brands = row[COLUMN_BRAND].replace(",", " ").split()
    mismatched_brands = set(row[COLUMN_TOKENIZED_DESCRIPTION]).isdisjoint(brands)
    return mismatched_brands and any(brands)


def _apply_bio_tagging(row):
    tokens = row[COLUMN_TOKENIZED_DESCRIPTION]
    good = row[COLUMN_GOOD].split(",")[0].split()
    brand = row[COLUMN_BRAND].split(",")[0].split()
    tags = [tag_to_index[TAG_O]] * len(tokens)
    for i, token in enumerate(tokens):
        if len(good) > 0 and tokens[i : i + len(good)] == good:
            tags[i] = tag_to_index[TAG_GOOD_BEGINNING]
            for j in range(i + 1, i + len(good)):
                tags[j] = tag_to_index[TAG_GOOD_INSIDE]
        if len(brand) > 0 and tokens[i : i + len(brand)] == brand:
            tags[i] = tag_to_index[TAG_BRAND_BEGINNING]
            for j in range(i + 1, i + len(brand)):
                tags[j] = tag_to_index[TAG_BRAND_INSIDE]
    return tags


def _format_for_mlm(
    row: pd.Series, use_targets: bool, ph_description, ph_good, ph_brand, ph_splitter
):
    result = ""
    description = " ".join(row[COLUMN_TOKENIZED_DESCRIPTION])
    if use_targets:
        # add placeholders for description, good and brand
        result = f"{ph_description}{description}{ph_splitter}{ph_good}{row[COLUMN_GOOD]}{ph_splitter}{ph_brand}{row[COLUMN_BRAND]}{ph_splitter}"
    else:
        # only use description
        result = f"{ph_description}{description}{ph_splitter}"
    return result


def _fix_word_languages(row: pd.Series, check_likely_mistakes: bool = True):
    # bias adjusts which language to prefer
    # > 0 for more russian -> english conversions
    words = row[COLUMN_TOKENIZED_DESCRIPTION]
    fixed_words = []
    for word in words:
        russian_letters = 0
        english_letters = 0
        word_bias = 0
        for char in word:
            if char in english_alphabet:
                english_letters += 1
                if char in likely_english_mistakes:
                    word_bias -= 1
                else:
                    word_bias += 1
            elif char in russian_alphabet:
                russian_letters += 1
                if char in likely_russian_mistakes:
                    word_bias += 1
                else:
                    word_bias -= 1
        if not check_likely_mistakes:
            word_bias = 0
        if (russian_letters == 0) or (english_letters == 0):
            # no need to do anything with non-mixed words
            fixed_words.append(word)
        elif (english_letters + word_bias) > russian_letters:
            # convert russian letters to english letters
            fixed_word = ""
            for char in word:
                if char in russian_alphabet:
                    fixed_word = "".join((fixed_word, russian_to_english[char]))
                else:
                    fixed_word = "".join((fixed_word, char))
            fixed_words.append(fixed_word)
        else:
            # convert english letters to russian letters
            fixed_word = ""
            for char in word:
                if char in english_alphabet:
                    fixed_word = "".join((fixed_word, english_to_russian[char]))
                else:
                    fixed_word = "".join((fixed_word, char))
            fixed_words.append(fixed_word)
    row[COLUMN_TOKENIZED_DESCRIPTION] = fixed_words
    return row


@click.command()
@click.argument("filepath", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
@click.option(
    "--classification-method",
    type=click.Choice(["tc", "mlm"]),
    required=True,
    help="The classification method to use.",
)
@click.option(
    "--use-targets/--no-use-targets",
    default=True,
    help="Whether to use the target columns for training. Default is True.",
)
@click.option(
    "--remove-uncommon-characters/--no-remove-uncommon-characters",
    default=True,
    help="Whether to remove uncommon characters from the description. Default is True.",
)
@click.option(
    "--fix-languages/--no-fix-languages",
    default=True,
    help="Whether to fix words with letters from different alphabets. Default is True.",
)
@click.option(
    "--check-likely-mistakes/--no-check-likely-mistakes",
    default=True,
    help="Whether to check for likely mistakes in words. Default is True.",
)
@click.option(
    "--drop-if-mismatched",
    type=click.Choice(["never", "all", "goods", "brands", "any"]),
    default="never",
    help="How to handle rows with mismatched goods or brands. Default is 'never'.",
)
@click.option(
    "--drop-input-columns/--no-drop-input-columns",
    default=True,
    help="Whether to drop the input columns after processing. Default is True.",
)
@click.option(
    "--desc-split-regex",
    default=r"[^a-zа-яё'&]+",
    help='The regex pattern to split the description into words. Default is r"[^a-zа-яё\'&]+".',
)
@click.option(
    "--desc-strip-chars",
    default="'\"&",
    help='The characters to strip from the description. Default is "\'"&".',
)
def prepare_data_cli(
    filepath,
    output_path,
    classification_method,
    use_targets,
    remove_uncommon_characters,
    fix_languages,
    check_likely_mistakes,
    drop_if_mismatched,
    drop_input_columns,
    desc_split_regex,
    desc_strip_chars,
):
    """
    Prepare the data for token classification or masked language modeling.
    """
    # Call the prepare_data function with the provided arguments
    df = prepare_data(
        filepath,
        classification_method,
        use_targets,
        remove_uncommon_characters,
        fix_languages,
        check_likely_mistakes,
        drop_if_mismatched,
        drop_input_columns,
        desc_split_regex,
        desc_strip_chars,
    )
    # Save the result as a csv
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    try:
        logging.info("Starting prepare_data...")
        prepare_data_cli()
    except Exception as e:
        logging.error("Error while preparing data: ")
        logging.exception(e)
    finally:
        logging.info("Finished prepare_data.")
