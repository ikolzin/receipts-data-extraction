import gc
import json
import logging
import os
from datetime import datetime, timedelta
from pathlib import Path
from timeit import default_timer as timer

import pandas as pd
import torch
import yaml

import src.data as data
import src.model as model

placeholder = "<>"

# This was used to locally run experiments defined with .yaml files.
# See mdeberta.yaml as an example.
# Not for production use.


def run_experiments():
    experiments_folder = "experiments/"
    experiment_files = sorted(os.listdir(experiments_folder))
    if len(experiment_files) == 0:
        logging.warn(f"No experiment files found in {experiments_folder}.")
    for experiment_file in experiment_files:
        try:
            experiment_path = Path(experiments_folder).joinpath(experiment_file)
            if not (experiment_path.is_file() and (experiment_path.suffix == ".yaml")):
                logging.info(f"Skipping {experiment_file}")
                continue
            with open(experiment_path, "r") as f:
                config = yaml.safe_load(f)
                experiment_name = config["experiment_name"]
                if experiment_name == placeholder:
                    logging.error(
                        f"Please specify experiment name for {experiment_file}. Skipping..."
                    )
                    continue
                results_output_dir = config["results_output_dir"]
                result_path = Path(results_output_dir).joinpath(
                    f"{datetime.now().date()}-{experiment_name}/"
                )

                logging.info(f"Launching experiment: {experiment_file}")
                run_experiment(config=config, result_path=result_path)

                # move finished experiment file to result folder
                os.makedirs(result_path, exist_ok=True)
                os.rename(
                    experiment_path,
                    result_path.joinpath(experiment_file),
                )
                logging.info(f"Finished experiment: {experiment_file}")

        except Exception as e:
            logging.error(f"Error while running experiment {experiment_file}: ")
            logging.exception(e)
        finally:
            # free resources for the next experiment
            gc.collect()
            if torch.cuda.is_available():
                torch.cuda.empty_cache()
    logging.info("Finished.")


def run_experiment(config: dict, result_path: str):
    logging.info(config)
    start_time = timer()

    # get global parameters
    experiment_name = config["experiment_name"]
    models_output_dir = config["models_output_dir"]

    # get subconfigs for experiment stages
    pre_traning_cfg = config["pre_training"]
    training_cfg = config["training"]
    test_on_train_cfg = config["test_on_train"]
    predict_cfg = config["predict"]

    eval_results_pretrain = ""
    eval_results_train = ""
    score = ""

    # fill placeholders
    # pre-training
    pre_trained_model_output = (
        Path(models_output_dir)
        .joinpath("pretrain")
        .joinpath(
            f"{pre_traning_cfg['train_model']['base_model_name']}-{experiment_name}"
        )
    )
    if pre_traning_cfg["train_model"]["output_dir"] == placeholder:
        pre_traning_cfg["train_model"]["output_dir"] = pre_trained_model_output

    # training
    if training_cfg["train_model"]["base_model_name"] == placeholder:
        # grab a pre-trained model as base
        training_cfg["train_model"]["base_model_name"] = pre_trained_model_output

    trained_model_output = Path(models_output_dir).joinpath(f"{experiment_name}")

    if training_cfg["train_model"]["output_dir"] == placeholder:
        training_cfg["train_model"]["output_dir"] = trained_model_output

    # test-on-train
    result_path_train = Path(result_path).joinpath("train")

    if test_on_train_cfg["predict"]["model_path"] == placeholder:
        test_on_train_cfg["predict"]["model_path"] = trained_model_output

    if test_on_train_cfg["predict"]["output_dir"] == placeholder:
        test_on_train_cfg["predict"]["output_dir"] = result_path_train

    # predict
    if predict_cfg["predict"]["model_path"] == placeholder:
        predict_cfg["predict"]["model_path"] = trained_model_output

    if predict_cfg["predict"]["output_dir"] == placeholder:
        predict_cfg["predict"]["output_dir"] = result_path

    # pre-training stage
    if pre_traning_cfg["enabled"] is True:
        logging.info("Starting pre-training")
        logging.info(pre_traning_cfg)
        df_pretraining_input = data.prepare_data(**pre_traning_cfg["prepare_data"])
        eval_results_pretrain = model.train_model(
            data=df_pretraining_input, **pre_traning_cfg["train_model"]
        )
        logging.info(eval_results_pretrain)
        eval_pretrain_path = Path(
            pre_traning_cfg["train_model"]["output_dir"]
        ).joinpath("eval.json")
        with open(eval_pretrain_path, "w") as f:
            json.dump(eval_results_pretrain, f, ensure_ascii=False, indent=4)
        logging.info("Finished pre-training")

    # training stage
    if training_cfg["enabled"] is True:
        logging.info("Starting training")
        logging.info(training_cfg)
        df_input = data.prepare_data(**training_cfg["prepare_data"])
        eval_results_train = model.train_model(
            data=df_input, **training_cfg["train_model"]
        )
        logging.info(eval_results_train)
        eval_train_path = Path(training_cfg["train_model"]["output_dir"]).joinpath(
            "eval.json"
        )
        with open(eval_train_path, "w") as f:
            json.dump(eval_results_train, f, ensure_ascii=False, indent=4)
        logging.info("Finished training")

    # make predictions on train file
    if test_on_train_cfg["enabled"] is True:
        logging.info(
            f"Starting test predictions on {test_on_train_cfg['prepare_data']['filepath']}"
        )
        logging.info(test_on_train_cfg)
        df_train = data.prepare_data(**test_on_train_cfg["prepare_data"])
        model.predict(input_df=df_train, **test_on_train_cfg["predict"])
        logging.info("Finished test predictions on train file")

    # calculate scores for train file
    if test_on_train_cfg["enabled"] is True:
        logging.info("Scoring results")
        results_train = Path(test_on_train_cfg["predict"]["output_dir"]).joinpath(
            "full.csv"
        )
        df_result_train = pd.read_csv(results_train).fillna("")
        df_orig = pd.read_csv(test_on_train_cfg["prepare_data"]["filepath"]).fillna("")
        df_result_train["orig_good"] = df_orig["good"]
        df_result_train["orig_brand"] = df_orig["brand"]
        df_result_train["orig_name"] = df_orig["name"]
        f1 = data.F1Score()
        f1.update(df_result_train["good"], df_result_train["orig_good"])
        good_score = f1.get()
        f1.reset()
        f1.update(df_result_train["brand"], df_result_train["orig_brand"])
        brand_score = f1.get()
        score = f"gs={good_score:.5}, bs={brand_score:.5}"
        score_path_train = Path(test_on_train_cfg["predict"]["output_dir"]).joinpath(
            score
        )
        os.makedirs(score_path_train, exist_ok=True)
        logging.info(f"Score for train file: {score}")

    # check which values differ between predictions and originals
    if test_on_train_cfg["enabled"] is True:
        logging.info("Recording difference between predictions and targets")
        q = "good != orig_good | brand != orig_brand"
        diff = df_result_train.query(q)[
            ["orig_name", "tokens", "good", "orig_good", "brand", "orig_brand"]
        ]
        diff_path_train = Path(test_on_train_cfg["predict"]["output_dir"]).joinpath(
            f"diff_{diff.shape[0]}.csv"
        )
        diff.to_csv(diff_path_train)
        logging.info(f"Number of different rows: {diff.shape[0]}")

    # make predictions for test file
    if predict_cfg["enabled"] is True:
        logging.info(
            f"Running predictions on {predict_cfg['prepare_data']['filepath']}"
        )
        logging.info(predict_cfg)
        df_test = data.prepare_data(**predict_cfg["prepare_data"])
        model.predict(input_df=df_test, **predict_cfg["predict"])
        logging.info("Finished running predictions")

    end_time = timer()
    duration = timedelta(seconds=end_time - start_time)
    with open(Path(config["results_output_dir"]).joinpath("log.tsv"), "a") as f:
        f.write(
            f"{experiment_name}\t{eval_results_pretrain}\t{eval_results_train}\t{score}\t{duration}\t{datetime.now()}\r\n"
        )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    run_experiments()
