from pathlib import Path
import yaml
from copy import deepcopy

base_experiment = {
    "experiment_name": "<>",
    "results_output_dir": "results/",
    "models_output_dir": "models/",
    "pre_training": {
        "enabled": False,
        "prepare_data": {
            "filepath": "data/initial/train_unsupervised_dataset.csv",
            "classification_method": "mlm",
            "use_targets": False,
        },
        "train_model": {
            "classification_method": "mlm",
            "base_model_name": "bert-base-multilingual-uncased",
            "output_dir": "<>",
        },
    },
    "training": {
        "enabled": True,
        "prepare_data": {
            "filepath": "data/initial/train_supervised_dataset.csv",
            "classification_method": "tc",
            "drop_if_mismatched": "all",
        },
        "train_model": {
            "classification_method": "tc",
            "base_model_name": "models/pretrain/bert-mlm",
            "output_dir": "<>",
        },
    },
    "test_on_train": {
        "enabled": True,
        "prepare_data": {
            "filepath": "data/modified/stage2_new_supervised.csv",
            "classification_method": "tc",
            "drop_input_columns": False,
        },
        "predict": {
            "model_path": "<>",
            "output_dir": "<>",
            "batch_size": 500,
        },
    },
    "predict": {
        "enabled": True,
        "prepare_data": {
            "filepath": "data/initial/test_dataset.csv",
            "classification_method": "tc",
            "drop_input_columns": False,
        },
        "predict": {
            "model_path": "<>",
            "output_dir": "<>",
            "batch_size": 500,
        },
    },
}

# This was used to batch create experiments using provided combinations of experiment_params with base_experiment as base.
# Experiment_params may contain single values or lists (then each value from the list will be a separate experiment).
# There is certainly a better way to do this, but it worked.


def create_experiments(experiment_params: dict, experiments_folder="experiments/"):
    # Apologies to anyone reading this.
    experiments = []
    name_key = "experiment_name"

    if experiment_params[name_key] in (None, "", "<>"):
        raise Exception("Please provide experiment name")

    experiment = base_experiment.copy()
    experiment[name_key] = experiment_params[name_key]

    experiments.append(experiment)

    for key, value in experiment_params.items():
        if isinstance(value, dict):
            for key_1, value_1 in value.items():
                if isinstance(value_1, dict):
                    for key_2, value_2 in value_1.items():
                        if isinstance(value_2, dict):
                            raise Exception("Dictionaries not supported at depth 3.")
                        elif isinstance(value_2, list):
                            new_experiments = []
                            for exp in experiments:
                                for new_val in value_2[1:]:
                                    new_exp = deepcopy(exp)
                                    new_exp[key][key_1][key_2] = new_val
                                    new_exp[
                                        name_key
                                    ] += f"_{key}-{key_1}-{key_2}-{new_val}"
                                    new_experiments.append(new_exp)
                                exp[key][key_1][key_2] = value_2[0]
                                exp[name_key] += f"_{key}-{key_1}-{key_2}-{value_2[0]}"
                            experiments.extend(new_experiments)
                        else:
                            for exp in experiments:
                                exp[key][key_1][key_2] = value_2
                elif isinstance(value_1, list):
                    new_experiments = []
                    for exp in experiments:
                        for new_val in value_1[1:]:
                            new_exp = deepcopy(exp)
                            new_exp[key][key_1] = new_val
                            new_exp[name_key] += f"_{key}-{key_1}-{new_val}"
                            new_experiments.append(new_exp)
                        exp[key][key_1] = value_1[0]
                        exp[name_key] += f"_{key}-{key_1}-{value_1[0]}"
                    experiments.extend(new_experiments)
                else:
                    for exp in experiments:
                        exp[key][key_1] = value_1
        elif isinstance(value, list):
            new_experiments = []
            for exp in experiments:
                for new_val in value[1:]:
                    new_exp = deepcopy(exp)
                    new_exp[key] = new_val
                    new_exp[name_key] += f"_{key}-{new_val}"
                    new_experiments.append(new_exp)
                exp[key] = value[0]
                exp[name_key] += f"_{key}-{value[0]}"
            experiments.extend(new_experiments)
        else:
            for exp in experiments:
                exp[key] = value

    for exp in experiments:
        name = exp[name_key]
        experiment_path = Path(experiments_folder).joinpath(f"{name}.yaml")
        with open(experiment_path, mode="w") as f:
            yaml.safe_dump(exp, f)
