from typing import Union
import mlflow
import logging
from random import randint
import os
from dotenv import load_dotenv
from pathlib import Path
import click


def test_run(artifacts_path: Union[Path, str, None] = None):
    """
    A method for testing MLFlow configuration.
    Runs an experiment named 'test' with some test parameters and metrics.
    If an artifact path is provided - saves folder contents as experiment artifacts.
    Logs artifact URI and endpoint URL to 'info' logging channel.

    Args:
        artifacts_path (Union[Path, str, None]): path to the folder containing artifacts to be logged.
    """
    EXPERIMENT_NAME = "test"
    load_dotenv()
    mlflow.set_experiment(EXPERIMENT_NAME)
    with mlflow.start_run():
        mlflow.log_params(
            {"test param 1": randint(0, 100), "test param 2": randint(-100, 0)}
        )
        mlflow.log_metrics(
            {"test metric 1": randint(0, 100), "test metric 2": randint(-100, 0)},
            step=1,
        )
        mlflow.log_metrics(
            {"test metric 1": randint(0, 100), "test metric 2": randint(-100, 0)},
            step=2,
        )
        mlflow.log_metrics(
            {"test metric 1": randint(0, 100), "test metric 2": randint(-100, 0)},
            step=3,
        )
        logging.info("Artifact URI: " + mlflow.get_artifact_uri())
        logging.info("Endpoint URL: " + str(os.environ.get("MLFLOW_S3_ENDPOINT_URL")))
        if artifacts_path is not None:
            mlflow.log_artifacts(
                local_dir=artifacts_path,
                artifact_path="test_artifacts",
            )


@click.command()
@click.option("--artifacts_path", type=click.Path(exists=True))
def test_run_command(artifacts_path: Union[Path, str, None] = None):
    test_run(artifacts_path=artifacts_path)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    test_run_command()
