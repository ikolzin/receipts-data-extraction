import gc
import logging
from pathlib import Path
from typing import Literal, Union

import mlflow
import torch
from dotenv import load_dotenv
import transformers

from src.data.prepare_data import prepare_data
from src.model.train_model import train_model


def train_model_experiment(
    experiment_name: str,
    data_file: Path | str,
    base_model: str,
    classification_method: Literal["tc", "mlm"],
    model_local_dir: Path | str,
    prepare_data_params: dict | None = None,
    train_model_params: dict | None = None,
    run_name: Union[str, None] = None,
    save_model_to_mlflow: bool = False,
):
    """
    Trains the model on provided data with specified parameters.
    Tracks the training as an experiment to MLFlow.

    Args:
        experiment_name (str): The name of the experiment to log to MLflow.
        data_file (Path | str): Path to the file containing training data.
        base_model (str): Name or path of the model to be used as baseline. Can be a local model or a model from the HF Hub.
        classification_method ("tc", "mlm"): Whether to train model for token classification or masked language modeling.
        model_local_dir (Path | str): Where to store model locally.
        prepare_data_params (dict | None): additional parameters to be passed to `src.data.prepare_data`. Defaults to None.
        train_model_params (dict | None): additional parameters to be passed to `src.model.train_model`. Defaults to None.
        run_name (str or None): The name of the training run. Defaults to None.
        save_msave_model_to_mlflowodel (bool): Whether or not to log the trained model to MLflow. Defaults to False.

    Returns:
        None
    """
    load_dotenv()
    logger = logging.getLogger(__name__)
    logger.info(f"Starting experiment {experiment_name}")

    try:
        mlflow.set_experiment(experiment_name=experiment_name)
        # log_models currently does nothing for HF models
        mlflow.transformers.autolog()

        with mlflow.start_run(run_name=run_name):
            mlflow.log_param("base_model", base_model)
            mlflow.log_param("classification_method", classification_method)

            if prepare_data_params is not None:
                mlflow.log_params(prepare_data_params)
            else:
                prepare_data_params = {}
            df = prepare_data(data_file, classification_method, **prepare_data_params)

            if train_model_params is not None:
                mlflow.log_params(train_model_params)
            else:
                train_model_params = {}

            metrics_total = train_model(
                df,
                classification_method,
                base_model,
                model_local_dir,
                **train_model_params,
            )

            if metrics_total is not None:
                for metrics_model in metrics_total:
                    mlflow.log_metrics(metrics_model)

            if save_model_to_mlflow:
                # to submit the model to mlflow we need to reload it
                gc.collect()
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
                tokenizer = transformers.AutoTokenizer.from_pretrained(
                    Path(model_local_dir)
                )
                if classification_method == "tc":
                    model = (
                        transformers.AutoModelForTokenClassification.from_pretrained(
                            model_local_dir
                        )
                    )
                    task = "token-classification"
                elif classification_method == "mlm":
                    model = transformers.AutoModelForMaskedLM.from_pretrained(
                        model_local_dir
                    )
                    task = "maskedLM"
                else:
                    raise Exception("Unsupported classification method")
                components = {
                    "model": model,
                    "tokenizer": tokenizer,
                }
                mlflow.transformers.log_model(
                    transformers_model=components,
                    artifact_path=model_local_dir,
                    task=task,
                )
        logger.info(f"Finished experiment {experiment_name}")

    except Exception as e:
        logger.error(f"Error while running experiment {experiment_name}: ")
        logger.exception(e)
    finally:
        # free resources for the next experiment
        gc.collect()
        if torch.cuda.is_available():
            torch.cuda.empty_cache()
